package scalajsreact.template

import japgolly.scalajs.react.{CtorType, _}
import japgolly.scalajs.react.component.Scala.Component
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.extra._
import japgolly.scalajs.react.raw.SyntheticKeyboardEvent

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import org.scalajs.dom.html

import scala.scalajs.js.JSON
import scala.util.Success

// template : https://github.com/chandu0101/scalajs-react-template//
// Exercises : https://egghead.io/courses/start-learning-react
object ReactApp {

  @JSExport
  def main(args: Array[String]): Unit = {

    object Exercise {

      @js.native
      trait SwapiAjaxResponse extends js.Object {
        val count: Int
        val next: String
        val previous: String
        val results: js.Array[People]
      }

      @js.native
      trait People extends js.Object {
        val name: String
        val height: String
        val mass: String
        val hair_color: String
        val skin_color: String
        val eye_color: String
        val birth_year: String
        val gender: String
        val homeworld: String
        val films: js.Array[String]
        val species: js.Array[String]
        val vehicles: js.Array[String]
        val starships: js.Array[String]
        val created: String
        val edited: String
        val url: String
      }

      case class State(names: Seq[String], filter: String)

      final case class Backend(bs: BackendScope[Unit, State]) {

        def updateFilter(e: ReactEventFromInput): CallbackTo[Unit] = {
          val targetValue = e.target.value
          bs.modState(s => State(s.names, targetValue))
        }

        def update(): CallbackTo[Unit] = {
          val ajax = Ajax("GET", s"https://swapi.co/api/people/?format=json")
            .setRequestContentTypeJsonUtf8
            .send
            .onComplete { xhr =>
              def names = JSON.parse(xhr.responseText).asInstanceOf[SwapiAjaxResponse].results.map(_.name)
              xhr.status match {
                case 200 => bs.modState(s => State(names, s.filter))
                case _ => bs.modState(s => State(Array("errooooorrrr"), s.filter))
              }
            }

          bs.modState(
            s => s,
            ajax.asCallback
          )
        }

        def render(state: State): VdomElement = {
          <.div(
            <.input(
              ^.`type` := "text",
              ^.onChange ==> updateFilter
            ),
            <.button(
              "Click me !",
              ^.onClick --> update()
            ),
            <.div(
              state.names.filter(n => n.toLowerCase().contains(state.filter.toLowerCase())).map(<.h4(_)): _*
            )
          )
        }
      }

      val Component: Component[Unit, Exercise.State, Exercise.Backend, CtorType.Nullary] = {
        ScalaComponent.builder[Unit]("App")
          .initialState(State(Seq(), ""))
          .renderBackend[Backend]
          .build
      }
    }

    <.div(
      Exercise.Component()
    ).renderIntoDOM(dom.document.getElementById("root"))
  }

}
